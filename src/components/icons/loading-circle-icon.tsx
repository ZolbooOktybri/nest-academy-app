import * as React from "react"
import Svg, { Defs, Path, Stop, LinearGradient } from "react-native-svg"

export const LoadingCircleIcon: React.FC<any> = () => {
    return (
        <Svg width="100%" height="100%" viewBox="0 0 49 48" fill="none">
            <Path
                d="M47.4998 23.9999C47.4998 36.1039 37.4421 45.9765 24.9599 45.9765C12.4777 45.9765 2.41992 36.1039 2.41992 23.9999C2.41992 11.896 12.4777 2.02344 24.9599 2.02344C37.4421 2.02344 47.4998 11.896 47.4998 23.9999Z"
                stroke="url(#paint0_linear)"
                strokeWidth="3" />
            <Defs>
                <LinearGradient
                    id="paint0_linear"
                    x1="35.853"
                    y1="2.35754"
                    x2="11.9429"
                    y2="45.7609"
                    gradientUnits="userSpaceOnUse">
                    <Stop stopColor="#00DCF0" />
                    <Stop offset="0.416667" stopColor="#172B4D" />
                </LinearGradient>
            </Defs>
        </Svg>
    )
}
