export enum NavigationRoutes {
    MainRoot = 'MainRoot',
    Home = 'Home',
    Course = 'Course',
    Profile = 'Profile',
    Test = 'Test',
    Splash = 'Splash'
}
export interface NavigationPayload<T> {
    props: T;
}
export type NavigatorParamList = {
    [NavigationRoutes.Splash]: NavigationPayload<any>;
    [NavigationRoutes.MainRoot]: NavigationPayload<any>;
    [NavigationRoutes.Home]: NavigationPayload<any>;
    [NavigationRoutes.Course]: NavigationPayload<any>;
    [NavigationRoutes.Profile]: NavigationPayload<any>;
    [NavigationRoutes.Test]: NavigationPayload<any>;
    [NavigationRoutes.Splash]: NavigationPayload<any>;
};
