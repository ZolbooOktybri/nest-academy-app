import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import MainBottomNavigation from './main-bottom-navigation';
import { SplashScreen } from '../screen';
import { NavigationRoutes, NavigatorParamList } from './navigation-params';

const RootStack = createStackNavigator<NavigatorParamList>();

export const RootNavigationContainer = () => {
    return (
        <NavigationContainer>
            <RootStack.Navigator initialRouteName={NavigationRoutes.Splash}>
                <RootStack.Screen
                    name={NavigationRoutes.Splash}
                    component={SplashScreen}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.MainRoot}
                    component={MainBottomNavigation}
                    options={{ headerShown: false }}
                />
            </RootStack.Navigator>
        </NavigationContainer>
    );
};
