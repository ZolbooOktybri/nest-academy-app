import React from 'react';
import { Image } from 'react-native';
import { Text, Button, Spacing, Box } from '../../components'
import { useNavigation } from '@react-navigation/native'
import { NavigationRoutes } from '../../navigation/navigation-params';

export const SplashThird = () => {
    const navigation = useNavigation()

    return (
        <Box flex={1} justifyContent='center' alignItems='center'>
            <Box flex={1} position={'absolute'} top={0} right={10} zIndex={5}>
                <Button category={'text'} size={'s'} status={'active'} onPress={() => navigation.navigate(NavigationRoutes.MainRoot)} width={95}>
                    <Text fontFamily={'Montserrat'} bold type={'body'}>АЛГАСАХ</Text>
                </Button>
            </Box>

            <Box flex={1}>
                <Spacing ph={15} pv={10} grow={1}>
                    <Box flex={1} justifyContent='center' alignItems='center'>
                        <Image source={require('../../assets/images/splash-third.png')} style={{ marginBottom: 25, marginTop: 20 }} />
                        <Spacing mt={8}>
                            <Text type={'title2'} bold textAlign={'center'}>Өөрийгөө сорь</Text>
                        </Spacing>
                        <Spacing mt={6} mb={9}>
                            <Text type={'body'} textAlign={'center'} role={'primary400'} width={250}>Та шинэ зүйлсийг туршиж үзэж өөрийгөө сорих боломжтой</Text>
                        </Spacing>
                    </Box>
                    <Button type={'primary'} category={'fill'} onPress={() => navigation.navigate(NavigationRoutes.MainRoot)} width={296}>
                        <Text fontFamily={'Montserrat'} bold type={'body'} role={"white"}>ЭХЭЛЦГЭЭЕ</Text>
                    </Button>
                </Spacing>
            </Box>
        </Box>
    );
}